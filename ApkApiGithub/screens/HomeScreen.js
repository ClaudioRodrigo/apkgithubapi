import React from 'react';
import { Alert, StyleSheet, Text, View, Image, TouchableOpacity, AsyncStorage, ScrollView } from 'react-native';
import ModalNovoRepositorio from '../componentes/ModalNovoRepositorio';
import CardRepositorios from '../componentes/CardRepositorios';

export default class HomeScreen extends React.Component {

  static navigationOptions = {
    title: 'Repositórios Github',
  };

  state = {
    modalVisible: false,
    repositorios: [],
  };

  async componentDidMount() {
    const repositorios = JSON.parse(await AsyncStorage.getItem("@RepositorioApp::repositorios")) || [];
    this.setState({ repositorios });
  };

  addRepositorio = async (ownerId, repositoryId) => {
    try {
      let response = await fetch(
        'https://api.github.com/repos/' + ownerId + '/' + repositoryId
      );

      if (response.ok) {
        let responseJson = await response.json();

        this.setState({
          modalVisible: false,
          repositorios: [
            ...this.state.repositorios,
            responseJson
          ].reverse()
        })
        await AsyncStorage.setItem("@RepositorioApp::repositorios", JSON.stringify([...this.state.repositorios]));
        Alert.alert("Cadastrado com Sucesso", "Seu repositório foi cadastrado com sucesso");
      } else {
        Alert.alert("Erro", "Por favor, preencha os campos corretamente ou verifique sua conexão");
      }
    } catch (error) {
      console.error(error);
    }
  };

  deleteRepository = async (key) => {

    this.state.repositorios.splice(key, 1);

    this.setState({
      repositorios: this.state.repositorios,
    })
    await AsyncStorage.setItem("@RepositorioApp::repositorios", JSON.stringify([...this.state.repositorios]));
  };

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          {this.state.repositorios.map((item, key) => (
            <CardRepositorios key={key}
              repositorio={item} onDelete={() => { this.deleteRepository(key) }}
              details={() => this.props.navigation.push('DetailsScreen', { item })
              }>
            </CardRepositorios>)
          )}
        </ScrollView>

        <View style={styles.viewButtonFooter}>
          <TouchableOpacity onPress={() => this.setState({ modalVisible: true })}>
            <Text style={styles.textFooterButton}>+</Text>
          </TouchableOpacity>
        </View>

        <ModalNovoRepositorio modalVisible={this.state.modalVisible}
          onCancel={() => this.setState({ modalVisible: false })}
          onAdd={this.addRepositorio}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10
  },
  header: {
    backgroundColor: 'black',
    paddingTop: 20,
    alignSelf: 'stretch',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingBottom: 5,
    borderBottomWidth: 3,
    borderBottomColor: '#C0C0C0',
  },
  headerText: {
    margin: 15,
    fontSize: 22,
    color: '#FFFF'
  },
  iconGithub: {
    width: 45,
    height: 45,
    padding: 10,
    marginLeft: 20,
    marginTop: 7,
  },
  viewButtonFooter: {
    position: 'absolute',
    marginLeft: 2,
    backgroundColor: '#CCC',
    justifyContent: 'center',
    alignItems: 'center',
    bottom: 0,
    left: 0,
    width: 55,
    height: 55,
    elevation: 10,
    zIndex: 10,
    marginBottom: 70,
    marginLeft: 250,
    borderRadius: 50,
  },
  textFooterButton: {
    fontSize: 25,
    borderRadius: 10,
    fontWeight: 'bold',
    borderRadius: 5,
    padding: 5,
  },
});
