import React from 'react';
import { Alert, StyleSheet, Text, View, Image, TouchableOpacity, AsyncStorage, ScrollView } from 'react-native';

export default class DetailsScreen extends React.Component {

  static navigationOptions = {
    title: 'Detalhes',
  };

  state = {
    owners: []
  };

  componentDidMount = async () => {
    
    try {
      let response = await fetch(
        'https://api.github.com/users/' + this.props.navigation.state.params.item.owner.login
      );
      if (response.ok) {
        let responseJson = await response.json();
        console.log(responseJson);
        this.setState({
          owners: [
            ...this.state.owners,
            responseJson
          ]
        })
      };
    } catch (error) {
      console.error(error);
    }
  };

  render() {

    return (
      <View style={styles.cardBody}>
        <View style={styles.headerCard}>
          <Text style={styles.textNomeRepositoy}>{this.props.navigation.state.params.item.name}</Text>
        </View>
        <View style={styles.conteudo}>
          <Image
            source={{ uri: this.props.navigation.state.params.item.owner.avatar_url }}
            style={styles.iconUser}
          />
          <Text style={styles.textLogin}>{this.props.navigation.state.params.item.owner.login}</Text>
        </View>
        <View style={styles.conteudoFooter}>
          <Text style={styles.textItem}> Id:
                <Text style={styles.textIValueItem}> {this.props.navigation.state.params.item.owner.id}</Text>
          </Text>
          <Text style={styles.textItem}> Descrição:
                <Text style={styles.textIValueItem}> {this.props.navigation.state.params.item.description}</Text>
          </Text>
          <Text style={styles.textItem}> Localização:
                <Text style={styles.textIValueItem}></Text>
          </Text>
        </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  cardBody: {
    margin: 10,
    marginBottom: 2,
    marginTop: 15,
    justifyContent: 'center',
    backgroundColor: '#FFF',
    borderRadius: 5,
    borderBottomWidth: 3,
    borderBottomColor: '#CCC',
    borderLeftWidth: 3,
    borderLeftColor: '#CCC',
    borderRightWidth: 1,
    borderRightColor: '#CCC',
    width: 320,
    height: 400,
  },
  headerCard: {
    borderTopLeftRadius: 4,
    borderTopRightRadius: 3,
    marginBottom: 5,
    height: 30,
    alignSelf: 'stretch',
    backgroundColor: 'black',
    alignItems: 'center',
  },
  conteudo: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
  },
  conteudoFooter: {
    flex: 1,
    paddingTop: 5,
    marginTop: 20,
    borderTopWidth: 1,
    borderTopColor: '#CCC',
  },
  iconUser: {
    width: 100,
    height: 100,
    borderRadius: 10,
  },
  buttonRemover: {
    padding: 10,
    marginLeft: 20,
    marginRight: 5,
    marginBottom: 7,
    width: 65,
    height: 30,
  },
  textNomeRepositoy: {
    color: '#FFFF',
    fontSize: 22,
    paddingBottom: 2,
    alignItems: 'center',
  },
  textLogin: {
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 14,
    fontStyle: 'italic'
  },
  textButtonRemover: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  textItem: {
    fontSize: 18,
    fontWeight: 'bold',
    margin: 5,
  },
  textIValueItem: {
    fontSize: 15,
    color: '#4D4D4D',
    margin: 5,
  },
});

