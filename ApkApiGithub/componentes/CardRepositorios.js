import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';


export default class CardRepositorios extends React.Component {
    render() {
        return (
            <TouchableOpacity onPress={this.props.details}>

                <View style={styles.cardBody}>
                    <View style={styles.headerCard}>
                        <Text style={styles.textNomeRepositoy}>{this.props.repositorio.name}</Text>
                    </View>
                    <View style={styles.conteudo}>
                        <Image
                            source={{ uri: this.props.repositorio.owner.avatar_url }}
                            style={styles.iconUser}
                        />
                        <Text style={styles.textLogin}>{this.props.repositorio.owner.login}</Text>
                        
                    </View>
                    <View style={styles.conteudoFooter}>
                        <Text style={styles.textId}> #ID:
                            <Text style={styles.textNumberId}> {this.props.repositorio.id}</Text>
                        </Text>
                        <View style={styles.buttonRemover}>
                            <TouchableOpacity onPress={this.props.onDelete}>
                                <Image
                                    source={{ uri: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAdVBMVEX///8AAADu7u5HR0cQEBD39/cxMTGUlJTl5eXU1NQrKytUVFRkZGRhYWG9vb24uLh6enre3t4/Pz/z8/PFxcVtbW1MTEyCgoKurq6ampoYGBjb29skJCQzMzPS0tJpaWmlpaUWFhaMjIx+fn4fHx+VlZU5OTmL/3AcAAAFh0lEQVR4nO2dbXu6LByGl+Vq2irNWivXmv+t7/8R77tlgIUiz7jjOt8a8DsnEwSBpycHrBenVV5+jShfZb46LdYuCrdPVuWjNvIq8x2eNsttq96V7dJ3iFosSoHfhXLhO0xlsrSH34V0oHX11NPvwsl3sCpsJARHo43vcKWJ2x+gfPLYd8hyRH0eMU3KQbWO8ZmjkGzTG9uEc/08pLt43whOpse4GX8cH6eTu19tPUWrwL9m5Ku2xiBbNX/4z2mUGiwaYb92NXbZvPHbgbT9sVTQRePXw/hXZKvefib8+WzPVmgH8WnzwgQ87nNP4jGT4sV6fPp8Mncw6pUiYu7ip+XoDJAp3BD2toffCX+jwe56J9rRRG8WYzMDjfVDItUHTWYtMkMwD//CfjIf0EqaSqWjL8uhV1PapZYbf1mSdImlyAwxo7WtX0txI6IJxZ0EnxwVKylbTY9WIjMFrWzvkinfFau3ayoSp+xrAn0hqaxEZgra65ata7R+h937pgNsB8mUB5Iy7GE3+kIr+0SkT+G5lchMAcN2YBgKMGwHhqEAw3aGYkgH2uRentjXJ+vDbcfNOVHlmU62SKclKSfPyuWfNz26w8xY2SARDoIsxXkEjuj1Un7uNjQE4zyxOIfg6Z4r+fuGT9++49PmW/B/uBNnETjC2ZKVOI+g6THKs5jnz0Mln/cc4ouHSj89AAAAAAAAAAAA/BGK03SonPp8nHq8X6szLCbCb8szcSaBI/oUvu/C1nARffbpOz4DdAtG4gyCRzC/7js8Awhq6dCnLcQTF/HQJ4ET4dD+Wm6VeWhs+qyXjrKXBw5k8fmkeLzqkIJ0SPLD49VM9iseBrJGcOJ3UfmaGI4N50wNNf5MBohgqAwMXQFDdWDoChiqA0NXwFAdGLoChurA0BUwVAeGroChOjB0RQiG8SzrGHCMDh3p14eZaMQ6AMPlZVpg1RLo4bJl5LZlQ4n4MmdSCpae+TesN2HhLyC7TTDzp2nrNZbd2754NyST5NyFjs/1Re4uYGRpZ+c0tXdDso9OyblIty/h1VMy6XXqKsC7YedKZ7plGeejEDr73Ll7i3fDVxInZ7U6XYHLWZ3Uc7U6DNWBYQ0MYQhDGGoAwxoYwhCGMNQAhjUwhCEMYagBDGtgCEMYwlADGNbAEIYwhKEGMKyBIQxhCEMNYFgDQxjCEIYawLAGhjCEIQw1gGENDGEIQxhqAMMaGMIQhjDUAIY1MIQhDGGoAQxrYAhDGMJQAxjWwBCG4RuSbXT2nItHInHkXN3fLnI377nh3XDRVT7dgoaXBymh8+Bp74ZPtzMUuLtdVfXFKe/ibQeivDN//4bR+fdXLUe4X8/OaNlE6Hpw/VmQv3fD/wNdfU45j5krxdv8rfVcrdn0cyU63T4EQ7vAUB0YugKG6vx9w+0tY9GxZpah/aKt4Zw7u9QOoR30V8M505O8WnYfdQTdXbJze0UFpiTnzo6xdUjXnt+51YC+21WGc5ajInEINquVhr7b8beXdUVC4uC9Y2pBchYeZWoT5iBY43mTBrH7HdwyZAzBeHPI7J/rs0Vkjknt3lBZBdoQGX9O94c5fdJCs8ycFNznbGgb0I2Ihaf+qkDbi9HET79mxhyobbqt+OWD5n+2kb9MANwdwbVhbuIod39WZz3GZfEWsg1GnzPMDfPCFG6hqbjSPHe9Eh68a5C4apRtrdPx3ijm62SrnAdOX42SzbeFhNdRk3QZ2b6TcbS7P9He9Jtho7h89MD3OLXH+PuxwNzqH3W9fyzRMXvLj/EoEcdglcR6rzi+/69wS+riCV6J47CGowGGohSHYoXSXZf/x4ugxWbwkfjH9X0sf1z2oX4di81EHJchJpvCtd+VbFelid0Wcp+k1U6rG/ofT3GBYf2K130AAAAASUVORK5CYII=' }}
                                    style={styles.iconLixeira}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    cardBody: {
        margin: 10,
        marginBottom: 2,
        marginTop: 15,
        justifyContent: 'center',
        backgroundColor: '#FFF',
        borderRadius: 5,
        borderBottomWidth: 3,
        borderBottomColor: '#CCC',
        borderLeftWidth: 3,
        borderLeftColor: '#CCC',
        borderRightWidth: 1,
        borderRightColor: '#CCC',
        width: 320,
        height: 180,
    },
    headerCard: {
        borderTopLeftRadius: 4,
        borderTopRightRadius: 3,
        marginBottom: 5,
        height: 30,
        alignSelf: 'stretch',
        backgroundColor: 'black',
        alignItems: 'center',
    },
    conteudo: {
        flex: 1,
        padding: 10,
    },
    conteudoFooter: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 5,
        marginTop: 30,
        borderTopWidth: 1,
        borderTopColor: '#CCC',
    },
    iconLixeira: {
        width: 25,
        height: 25,
        marginLeft: 10,
    },
    buttonRemover: {
        padding: 10,
        marginLeft: 20,
        marginRight: 5,
        marginBottom: 7,
        width: 65,
        height: 30,
    },
    textNomeRepositoy: {
        color: '#FFFF',
        fontSize: 22,
        paddingBottom: 2,
        alignItems: 'center',
    },
    textLogin: {
        marginLeft: 10,
        fontSize: 14,
        fontStyle: 'italic'
    },
    textButtonRemover: {
        fontSize: 18,
        fontWeight: 'bold',
    },
    textId: {
        fontSize: 18,
        fontWeight: 'bold',
        margin: 5,
    },
    textNumberId: {
        fontSize: 15,
        color: '#1F1F1B',
        margin: 5,
    },
    iconUser: {
        paddingLeft: 20,
        marginLeft: 10,
        width: 50,
        height: 50,
        borderRadius: 10,
    },
});
