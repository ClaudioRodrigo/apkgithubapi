import React from 'react';
import { StyleSheet, View, Modal, TextInput, TouchableOpacity, Text, Picker } from 'react-native';

export default class ModalNovoRepositorio extends React.Component {

    state = {
        ownerId: '',
        repositoryId: '',
    };

    onPressAddRepositorio = () => {
        this.props.onAdd(this.state.ownerId, this.state.repositoryId);

        this.setState({
            ownerId: '',
            repositoryId: '',
        });
    };

    render() {
        return (

            <Modal visible={this.props.modalVisible} animationType={'fade'} transparent={true} onRequestClose={this.props.onCancel}>

                <View style={styles.container}>
                    <View style={styles.containerBox}>
                        <View style={styles.headerModal}>
                            <Text style={styles.textHeaderModal}>Adicione seu Repositório</Text>
                        </View>
                        <TextInput style={styles.boxInput}
                            autoFocus
                            placeholder="Nome do usuário Ex.: claudiozh "
                            value={this.state.ownerId}
                            onChangeText={ownerId => this.setState({ ownerId })}
                        />
                        <TextInput style={styles.boxInput}
                            placeholder="Nome do projeto Ex.: nutriSistem"
                            value={this.state.repositoryId}
                            onChangeText={repositoryId => this.setState({ repositoryId })}
                        />
                        <View style={styles.butonsContainer}>
                            <TouchableOpacity onPress={this.props.onCancel} style={[styles.button, styles.buttonCancel]}>
                                <Text style={styles.buttonText}>Cancelar</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.onPressAddRepositorio} style={[styles.button, styles.buttonAdd]}>
                                <Text style={styles.buttonText}>Adicionar</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal >
        )
    };
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 2,
    },
    headerModal: {
        padding: 20,
    },
    textHeaderModal: {
        fontWeight: 'bold',
        fontFamily: 'Roboto',
        fontSize: 20,
    },
    containerBox: {
        borderRadius: 10,
        backgroundColor: '#FFFF',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 25.
    },
    buttonAdd: {
        backgroundColor: '#00AE15',
        borderBottomColor: '#C0C0C0',
        borderRightColor: '#C0C0C0'
    },
    buttonCancel: {
        backgroundColor: '#D46A6A',
        
        borderBottomColor: '#C0C0C0',
        borderRightColor: '#C0C0C0'
    },
    button: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        margin: 10,
        borderBottomWidth: 3,
        borderRightWidth: 3,
    },
    butonText: {
        fontWeight: 'bold',
    },
    butonsContainer: {
        flexDirection: 'row',
        marginTop: 10,
        height: 60,
    },
    boxInput: {
        alignSelf: 'stretch',
        height: 40,
        margin: 20,
        borderRadius: 5,
    }
});  