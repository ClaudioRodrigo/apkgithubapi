import React from 'react';
import { Alert, StyleSheet, Text, View, Image, TouchableOpacity, AsyncStorage, ScrollView } from 'react-native';


export default class LogoTitle extends React.Component {
    render() {
        return (
            <Image
                source={require('../assets/github.png')}
                style={{ width: 35, height: 35, marginTop: 5, marginRight: 20 }}
            />
        );
    }
}
