/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, AsyncStorage, ScrollView } from 'react-native';
import HomeScreen from './screens/HomeScreen';
import DetailsScreen from './screens/DetailsScreen';
import { createStackNavigator } from 'react-navigation';
import LogoTitle from './componentes/LogoTitle';

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <RootStack />
    );
  }
}

const RootStack = createStackNavigator(
  {
    HomeScreen: HomeScreen,
    DetailsScreen: DetailsScreen,
  },
  {
    initialRoutName: 'HomeScreen',
    navigationOptions: {
      headerStyle: {
        backgroundColor: 'black',

      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
        fontSize: 22,
        marginLeft: 80,
      },
      headerRight: <LogoTitle />,
    },
  }
);

